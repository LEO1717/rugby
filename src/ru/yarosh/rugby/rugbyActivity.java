package ru.yarosh.rugby;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class rugbyActivity extends Activity {
    private Integer int_first_com = 0;
    private Integer int_second_com = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public int upAttempt(){
        return int_first_com+=5;
    }

    public void upCommand(View view){
        String str_first_command = "";
        String str_second_command = "";
        int recourcesColor=0;

        TextView txt_first_command = (TextView) findViewById(R.id.txt_first_command);
        str_first_command = txt_first_command.getText().toString();
        int_first_com = Integer.parseInt(str_first_command);

        TextView txt_second_command = (TextView) findViewById(R.id.txt_second_command);
        str_second_command = txt_second_command.getText().toString();
        int_second_com = Integer.parseInt(str_second_command);

        LinearLayout mainLayout = (LinearLayout) findViewById(R.id.mainLayout);


        switch(view.getId()){
            case R.id.attemptFirst: {
                int_first_com+=5;
                txt_first_command.setText(int_first_com.toString());
                recourcesColor=R.color.background_first;
                break;
            }
            case R.id.attemptSecond:{
                int_second_com+=5;
                txt_second_command.setText(int_second_com.toString());
                recourcesColor=R.color.background_second;
                break;
            }
            case R.id.implementationFirst:{
                int_first_com+=2;
                txt_first_command.setText(int_first_com.toString());
                recourcesColor=R.color.background_first;
                break;
            }
            case R.id.implementationSecond:{
                int_second_com+=2;
                txt_second_command.setText(int_second_com.toString());
                recourcesColor=R.color.background_second;
                break;
            }
            case R.id.penaltyFirst:{
                int_first_com+=3;
                txt_first_command.setText(int_first_com.toString());
                recourcesColor=R.color.background_first;
                break;
            }
            case R.id.penaltySecond:{
                int_second_com+=3;
                txt_second_command.setText(int_second_com.toString());
                recourcesColor=R.color.background_second;
                break;
            }
            case R.id.dropFirst:{
                int_first_com+=3;
                txt_first_command.setText(int_first_com.toString());
                recourcesColor=R.color.background_first;
                break;
            }
            case R.id.dropSecond:{
                int_second_com+=3;
                txt_second_command.setText(int_second_com.toString());
                recourcesColor=R.color.background_second;
                break;
            }

        }
        if(int_first_com == int_second_com) mainLayout.setBackgroundColor(getResources().getColor(R.color.background));
        else mainLayout.setBackgroundColor(getResources().getColor(recourcesColor));

    }
}
